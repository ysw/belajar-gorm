package entity

import (
	"database/sql"
	"time"
)

type User struct {
	ID           uint   `gorm:"primaryKey"`
	Name         string `json:"name"`
	Email        string `json:"email"`
	Age          uint8  `json:"age"`
	Birthday     string `json:"birthday"`
	MemberNumber sql.NullString
	ActivatedAt  sql.NullTime
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
