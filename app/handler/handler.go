package handler

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type HandlerUser interface {
	Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params)
}
