package handler

import (
	"github.com/julienschmidt/httprouter"
)

func Router(userHandler HandlerUser) *httprouter.Router {
	router := httprouter.New()
	router.POST("/create", userHandler.Create)

	return router
}
