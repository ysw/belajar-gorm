package users

import (
	"belajar-gorm/app/entity"
	"belajar-gorm/app/services"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type Handler struct {
	UCaseUser services.ServiceUser
}

func NewHandler(UCaseUser services.ServiceUser) *Handler {
	return &Handler{UCaseUser: UCaseUser}
}

func (h Handler) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var (
		user    = entity.User{}
		status  int
		message string
	)

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err := h.UCaseUser.Create(user)

	w.Header().Set("Content-Type", "application/json")

	if err != nil {
		status = 1
		message = "insert failed"
	} else {
		status = 0
		message = "insert success"
	}

	response := entity.MessageResponse{
		Status:  status,
		Message: message,
	}

	err = json.NewEncoder(w).Encode(response)

	return
}
