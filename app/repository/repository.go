package repository

import "belajar-gorm/app/entity"

type RepositoryUser interface {
	Create(user entity.User) error
	Update(user entity.User) error
	Delete(int int) error
}
