package users

import (
	"belajar-gorm/app/entity"
	"errors"
	"gorm.io/gorm"
)

type Users struct {
	Db *gorm.DB
}

func NewUsers(db *gorm.DB) *Users {
	return &Users{
		Db: db,
	}
}

func (u Users) Create(req entity.User) error {
	user := entity.User{
		Name:     req.Name,
		Age:      req.Age,
		Birthday: req.Birthday,
	}

	u.Db.Create(&user) // pass pointer of data to Create

	if user.ID < 0 {
		return errors.New("insert failed")
	}

	return nil
}

func (u Users) Update(user entity.User) error {
	//TODO implement me
	panic("implement me")
}

func (u Users) Delete(int int) error {
	//TODO implement me
	panic("implement me")
}
