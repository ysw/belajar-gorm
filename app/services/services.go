package services

import "belajar-gorm/app/entity"

type ServiceUser interface {
	Create(user entity.User) error
}
