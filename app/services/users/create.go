package users

import (
	"belajar-gorm/app/entity"
	repository "belajar-gorm/app/repository"
)

type Users struct {
	Repo repository.RepositoryUser
}

func NewUsers(repo repository.RepositoryUser) *Users {
	return &Users{
		Repo: repo,
	}
}

func (u Users) Create(user entity.User) error {
	err := u.Repo.Create(user)

	return err
}
