package main

import (
	router "belajar-gorm/app/handler"
	handler "belajar-gorm/app/handler/users"
	repository "belajar-gorm/app/repository/users"
	service "belajar-gorm/app/services/users"
	pkg "belajar-gorm/pkg/database"
	"fmt"
	"net/http"
)

func main() {
	db, err := pkg.DBConnect()
	if err != nil {
		panic(err)
	}
	repositoryImpl := repository.NewUsers(db)
	serviceImpl := service.NewUsers(repositoryImpl)
	handlerImpl := handler.NewHandler(serviceImpl)
	rt := router.Router(handlerImpl)

	server := http.Server{
		Addr:    "localhost:3000",
		Handler: rt,
	}

	e := server.ListenAndServe()
	fmt.Println("Server Running")

	if e != nil {
		panic(e)
	}
}
